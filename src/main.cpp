#include <Arduino.h>

uint32_t super_secret_password = 1234;
uint32_t password = 4321;

void setup()
{
  Serial.begin(115200);
  if (password != super_secret_password)
  {
    Serial.println("You have NO ACCESS!");
    while (1) ;
  }
  Serial.println("You have ACCESS!");
}

void loop()
{
  Serial.print("The answer is: 42...");
}