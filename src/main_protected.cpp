#include <Arduino.h>

uint32_t super_secret_password = 1234;
uint32_t password = 4321;

/* Global failure loop for bootloader code. Uses attribute used to prevent
 * compiler removing due to non-standard calling procedure. Multiple loop jumps
 * used to make unlooping difficult.
 */
__attribute__((used))
__attribute__((noinline)) void
protected_infinite_loop(void)
{
  __asm volatile("b protected_infinite_loop");
  __asm volatile("b protected_infinite_loop");
  __asm volatile("b protected_infinite_loop");
  __asm volatile("b protected_infinite_loop");
  __asm volatile("b protected_infinite_loop");
  __asm volatile("b protected_infinite_loop");
  __asm volatile("b protected_infinite_loop");
  __asm volatile("b protected_infinite_loop");
  __asm volatile("b protected_infinite_loop");
}

void setup()
{
  Serial.begin(115200);
  if (password != super_secret_password)
  {
    Serial.println("You have NO ACCESS!");
    protected_infinite_loop();
  }
  Serial.println("You have ACCESS!");
}

void loop()
{
  Serial.print("The answer is: 42...");
}